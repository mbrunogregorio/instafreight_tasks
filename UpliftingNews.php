<?php

class UpliftingNew
{
    public $title;
    public $permalink;
    public $thumbnail;

    public function __construct($attributes = array())
    {

        if (!empty($attributes)) {
            $this->title = $attributes['data']['title'];
            $this->url = $attributes['data']['url'];
            $this->thumbnail = $attributes['data']['thumbnail'];
        }
    }

    public function __toString()
    {
        return "<li><a href='$this->url'>" . $this->title . "</a></li>";
    }

}

class UpliftingNewsPage
{

    public function getNews()
    {

        $url = 'https://www.reddit.com/r/UpliftingNews/.json';
        try {
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_USERAGENT, true);
            $result = curl_exec($ch);
            $info = curl_getinfo($ch);
            curl_close($ch);

            if ($info['http_code'] == 200) {
                $result_array = json_decode($result, true);
                $list_news = array();
                $news = $result_array['data']['children'];
                foreach ($news as $retrieved_new) {
                    $new = new UpliftingNew($retrieved_new);
                    #xdebug_var_dump($retrieved_new);
                    $list_news[] = $new;
                }
                return $list_news;
            } else {
                throw new Exception();
            }
        } catch (Exception $exception) {
            return $exception->getMessage();
        }

    }

}

$up_news = new UpliftingNewsPage();
$news = $up_news->getNews();
foreach ($news as $new) {
    echo $new . PHP_EOL;
}

//======================== What further improvements would you implement?
// 1- Avoid 2 endpoint, one fot JSON response and other for XML response, by using the Content Negotiation concept,
// allow the clients to specify the content format in the response in the request header.
//
// 2-Add sort and query params at the endpoint, to allow the clients order and filter the collection

//======================== What can we do in order to reduce requests? Many users are visiting the homepage and it's becoming slow
// The first step is to use a cache storage, to avoid the API Rest request at each client request
// so, we can save the collections in the cache(file).
// I would suggest to use memcached to keep the records ready to be delivered to the clients and spend less server processement