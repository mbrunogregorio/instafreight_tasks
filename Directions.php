<?php

class Point
{
  /** @var float */
  private $lat;

  /** @var float */
  private $lng;

  /**
   * @param float $lat
   * @param float $lng
   */
  public function __construct($lat, $lng)
  {
    $this->lat = $lat;
    $this->lng = $lng;
  }

  /**
   * @return string
   */
  public function __toString()
  {
    return '(' . $this->lat . ', ' . $this->lng . ')';
  }
}

class RouteDecoder
{

  private $encodedRoute;
  private $index = 0;
  private $b;
    /**
     * @param string $encodedRoute
     * @return Point[]
     */
    public function decode($encodedRoute)
    {
      // @TODO port decodeRoute from Javascript
      $this->encodedRoute = $encodedRoute;
      $len = strlen($this->encodedRoute);

      $array = [];
      $lat = 0;
      $lng = 0;

      while ($this->index < $len)
      {

        $this->b = '';
        $lat = $lat+$this->decodeSingle();
        $lng = $lng+$this->decodeSingle();
        $point = new Point($lat * 1e-5, $lng * 1e-5);

        array_push($array, $point);
      }
        return $array;
    }

    /**
    * @param string $string
    * @param integer $offset
    * @return $code
    */
    public function charCodeAt($string, $offset)
    {
      $string = substr($string, $offset, 1);
      $code = ord($string);

      return $code;
    }

    function decodeSingle()
    {
      $shift = 0;
      $result = 0;

      do
      {
        $this->b = $this->charCodeAt($this->encodedRoute, $this->index++) - 63;
        $result |= ($this->b & 0x1f) << $shift;
        $shift += 5;
      } while ($this->b >= 0x20);
      $delta = (($result & 1) ? !($result >> 1) : ($result >> 1));

      return $delta;
    }
}


$routeDecoder = new RouteDecoder();
$points = $routeDecoder->decode('mkk_Ieg_qAiPePsHd[}CzMq@`CaAfCwCvLyApG[xBKZyCpPaDjQ');

echo 'Route has ', sizeof($points), ' points', PHP_EOL;

foreach ($points as $point)
{
  echo $point, PHP_EOL;
}
