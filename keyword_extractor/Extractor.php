<?php

class Extractor
{

    public $rank = array();
    public $extracted_words = array();
    public $title = array();
    public $stop_words = array();
    public $path = 'files/';

    /* **
       INITIALIZE THE EXTRACTED WORDS ARRAY BY GETTINT THE CONTENT OF ALL "postN" file
     */
    public function getWords()
    {

        $directory = dir($this->path);
        while ($file = $directory->read()) {
            $file_name = substr($file, 0, 4);
            if ($file_name === "post")
                $files[] = $file;
        }
        $directory->close();

        foreach ($files as $to_extract) {
            $content = file_get_contents($this->path . $to_extract);
            $this->extracted_words = array_merge($this->extracted_words, $this->contentToArray($content));
            $this->getTitle($content);
        }

    }

    /* **
       OPEN STOPWORD FILE, TAKE THE CONTENT AND PUT EVERYWORD IN AN ARRAY
     */
    public function getStopWords()
    {

        $file = fopen($this->path . "stopwords.txt", "r") or die("Unable to open file!");
        while (!feof($file)) {
            $word = $this->removeLnBreak(fgets($file));
            $word = htmlentities($word, ENT_NOQUOTES, 'utf-8');
            $word = str_replace("'", "&rsquo;", $word);
            $this->stop_words[] = $word;
        }
        fclose($file);


    }

    /* **
       REMOVE LINE BREAKS, POINTS, COMMA, HASHTAG AND TRANSFORM TO LOWER CASE
     */
    public function removeLnBreak($word)
    {
        $word = $this->sanitizeWord($word);
        $word = strtolower($word);
        $word = str_replace("\n", "", $word);
        $word = str_replace("\r", "", $word);
        $word = preg_replace('/\s/', ' ', $word);
        return $word;
    }

    /* **
        RECEIVE THE POST CONTENT AND RETURNS AN ARRAY WITH EVERY WORD
     */
    public function contentToArray($content)
    {
        $content = htmlentities($content, ENT_QUOTES, 'utf-8');
        $content = $this->removeLnBreak($content);
        $content = explode(' ', $content);
        return $content;
    }

    public function getTitle($content)
    {
        $lines = explode("\n", $content);
        #xdebug_var_dump($lines);
        foreach ($lines as $line) {
            if ($line[0] == '#') {
                $title = $this->removeLnBreak($line);
                $this->title = array_merge($this->title, explode(' ', $title));
            }
        }
    }


    public function calcOccurrences()
    {
        //go through the extracted_words array
        //if the word is a key on rank array $rank[$word]++
        //if the word is a key on title array $rank[$word]+2
        //else create $rank[$word] = 1
        foreach ($this->extracted_words as $word) {
            //Exclude the stopword.txt words
            if (!in_array($word, $this->stop_words)) {
                if (array_key_exists($word, $this->rank)) {
                    if (array_key_exists($word, $this->title)) {
                        $this->rank[$word] = $this->rank[$word] + 2;
                    } else {
                        $this->rank[$word]++;
                    }
                } else
                    $this->rank[$word] = 1;
            }
        }
        arsort($this->rank, SORT_NUMERIC);
    }

    public function sanitizeWord($word)
    {
        return preg_replace('/[.#,]/', '', $word);
    }
}


$ext = new Extractor();

$ext->getWords();
$ext->getStopWords();
$ext->calcOccurrences();

var_dump(array_slice($ext->rank,0,10));
